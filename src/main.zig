const std = @import("std");
const Easy = @import("curl").Easy;
// const url = "https://agdsn.de/sipa/hotline-fragment";
const url = "http://localhost:5001/news/";

pub fn main() !void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();

    const curl = try Easy.init(arena.allocator());
    defer curl.deinit();

    const resp = try curl.get(url);
    defer resp.deinit();

    const items = resp.body.items;
    std.debug.print("Status code: {}\nFirst 100 bytes of body: {s}", .{
        resp.status_code,
        items[0..@min(items.len, 1000)],
    });
}
